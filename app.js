
var express = require('express');
var app = express();
var logic = require('./logic');
var bodyParser = require('body-parser');

var api = {
  base: 'localhost:3000',
  get: {
    currencies: '/currencies'
  },
  post: {
    exchange: '/exchange'
  }
};

app.use(express.static('public'));

app.use(bodyParser.json());

app.get('/', function (req, res) {
  
  res.sendFile(__dirname + '/views/index.html');
});

app.get('/api', function(req, res) {

  return res.json(api);
});

app.get(api.get.currencies, logic.currencies);

app.post(api.post.exchange, logic.exchange);

app.listen(3000, function () {
  console.log('currex is listening on port 3000!');
});
