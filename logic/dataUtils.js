
var fs = require('fs');

var exports = {
  currencies: function(callback) {

    getData(function (err, data) {

      if (err) {
        console.error(err);
        return callback('Could not read data file');
      }

      data = JSON.parse(data);

      if(data.currencies) {

        return callback(null, data.currencies);
      }

      return callback('No currencies in data');

    });
  },
  exchangeRates: function(callback) {
    getData(function (err, data) {

      if (err) {
        console.error(err);
        return callback('Could not read data file');
      }

      data = JSON.parse(data);

      if(data.rates) {

        return callback(null, data.rates);
      }

      return callback('No rates in data');
    });
  }
};

function getData(callback) {
  fs.readFile(__dirname + '/../data.json', 'utf8', callback);
}

module.exports = exports;
