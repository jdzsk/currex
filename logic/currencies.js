
var dataUtils = require('./dataUtils.js');

function getCurrencies(req, res) {

  dataUtils.currencies(function(err, currencies) {

    if(err) {
      return res.status(500).send(err);
    }

    return res.json(currencies);
  });
}

module.exports = getCurrencies;
