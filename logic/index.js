
var currencies = require('./currencies.js');
var exchange = require('./exchange.js');

var exports = {
  currencies: currencies,
  exchange: exchange
};

module.exports = exports;
