
var dataUtils = require('./dataUtils.js');
var _ = require('lodash');
var waterfall = require('async/waterfall');

function postExchange(req, res) {

  var currencyFrom = req.body.currencyFrom,
  currencyTo = req.body.currencyTo,
  amount = req.body.amount;

  if(currencyTo === currencyFrom || amount === 0) {

    return res.json({amount: amount});
  }

  waterfall([
    dataUtils.exchangeRates,
    function(rates, callback) {

      var directExchange = _.find(rates, function(rate) {

        return _.includes(rate.currencies, currencyFrom) && _.includes(rate.currencies, currencyTo);
      });

      if(directExchange) {

        return callback(null, directExchange, null);
      }

      var filteredRates = findRates(rates, currencyFrom, currencyTo);

      return callback(null, null, filteredRates);
    },
    function(directExchange, filteredRates, callback) {

      var result = {};

      if(directExchange) {
        var params = {
          amount: amount,
          multiply: directExchange.currencies[0] === currencyFrom,
          rate: directExchange.rate
        };

        result = {
          amount: getExchangeAmount(params)
        };

        return callback(null, result);
      }

      var currFrom = currencyFrom;

      _(filteredRates).each(function(rate) {

        var params = {
          amount: result.amount || amount,
          multiply: rate.currencies[0] === currFrom,
          rate: rate.rate
        };

        result = {
          amount: getExchangeAmount(params)
        };

        currFrom = _.difference(rate.currencies, [currencyFrom])[0];
      });

      return callback(null, result);
    }
  ],
  function(err, finalResult) {

      if(err) {
        console.error(err);
        return res.status(500).send(err);
      }

      return res.json(finalResult);
  });
}

function getExchangeAmount(params) {

  if(params.multiply) {
    return (params.amount * params.rate).toFixed(2);
  }

  return (params.amount / params.rate).toFixed(2);
}

function findRates(rates, currFrom, currTo) {
  console.log('rates', rates);

  var result = [];

  var ratesFrom = _.filter(rates, function(rate) {

    return _(rate.currencies).includes(currFrom);
  });

  console.log('ratesFrom', ratesFrom);

  var ratesTo = _.filter(rates, function(rate) {

    return _(rate.currencies).includes(currTo);
  });

  console.log('ratesTo', ratesTo);

  _(ratesFrom).each(function(rateFrom) {

    var midCurrency = _.difference(rateFrom.currencies, [currFrom])[0];

    console.log('mid currency', midCurrency);

    var midRate = _.find(ratesTo, function(rateTo) {

      return _(rateTo.currencies).includes(midCurrency);
    });

    if(midRate) {

      result.push(rateFrom);
      result.push(midRate);
      return false;
    }
  });

  console.log(result);
  return result;
}

module.exports = postExchange;
