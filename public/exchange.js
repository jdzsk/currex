(function() {

  var app = angular.module('exchange', []);

  app.controller('ExchangeController', ['$scope','$http', function($scope, $http) {

    var api;
    var that = this;

    this.currencies = [];
    this.amount = 0;

    $http.get('/api')
      .then(function success(response) {

        api = response.data;
        getCurrencies();
      }, function error(response) {

        console.error(response);
      });

    function getCurrencies() {

      $http.get(api.get.currencies)
        .then(function success(response) {

          that.currencies = response.data;
          that.currencyFrom = that.currencies[0];
          that.currencyTo = that.currencies[0];
        }, function error(response) {

          console.error(response);
        });
    }

    this.swap = function(currencyFrom, currencyTo) {

      that.currencyTo = currencyFrom;
      that.currencyFrom = currencyTo;
    };

    this.doExchange = function(currencyFrom, currencyTo, amount) {

      if(isNaN(amount)) {
        console.log('given amount is not a number', amount);
        return;
      }

      if(amount === null) {

        that.amount = 0;
        amount = 0;
      }

      var data = {

        currencyFrom: currencyFrom,
        currencyTo: currencyTo,
        amount: amount
      };

      $http.post(api.post.exchange, data)
        .then(function success(response) {

          that.result = response.data.amount;
        }, function error(response) {

          console.error(response);
        });
    };

  }]);


  app.directive('numberOnlyInput', function () {
    return {
        template: '<input required="required" step="any" name="amount" ng-model="exchange.amount" type="number">'
    };
});
})();
